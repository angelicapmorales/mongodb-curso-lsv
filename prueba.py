from bson.objectid import ObjectId  # noqa

from car.databases.client import MongoLsv

mongo_lsv = MongoLsv()
print(mongo_lsv.list_dbs())
print(mongo_lsv.list_collections(db_name="test_mongo"))
print(
    mongo_lsv.get_records_from_collection(
        db_name="test_mongo", collection="nombres"
    )
)
""" print(
    mongo_lsv.update_record_in_collection(
        db_name="car_lsv",
        collection="documents_types",
        record_query={"_id": ObjectId("62a947ef974907fd6a728a5d")},
        record_new_value={"label": "Registro Civil"},
    )
) """

""" print(
    mongo_lsv.delete_record_in_collection(
        db_name="car_lsv",
        collection="documents_types",
        record_id="62a947ef974907fd6a728a5d",
    )
) """

""" print(
    mongo_lsv.create_new_record_in_collection(
        db_name="test_mongo",
        collection="nombres",
        record={"label": "Nombre_persona", "value": "Angela Maria"},
    )
)  """

""" print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="students",
        record={"first_name": "Angel", "last_name": "Mora","document_number":"12345678", "documents_types":"ObjectId('62a81cd20ab3472faf03d952')","created_at":"2022-06-11","modified_at":"2022-06-12"},
    )
) """


