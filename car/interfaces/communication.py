from dataclasses import asdict, dataclass


@dataclass
class TypeOfMedia:
    uuid: str
    name: str

    type_of_media = {"name":"Medios Masivos"}

    def to_dict(self) -> dict:
        return asdict(self)
    
class Media:
    uuid: str
    name: str
    type_of_media_id: str

    media={"name":"internet", "type_of_media":"type_of_media_id"}

    def to_dict(self) -> dict:
        return asdict(self)