from bson.objectid import ObjectId
import sys
sys.path.append("..")
from databases.client import MongoLsv

mongo_lsv = MongoLsv()

print(
    mongo_lsv.update_record_in_collection(
        db_name="communications",
        collection="type_of_media",
        record_query={"_id": ObjectId("62afb84a1214775c067995e2")},
        record_new_value={"name": "Medios Auxiliares"},
    )
) 

print(
    mongo_lsv.update_record_in_collection(
        db_name="communications",
        collection="media",
        record_query={"_id": ObjectId("62afbab040e36081bcba1e8b")},
        record_new_value={"name": "Revistas o magazines"},
    )
) 

print(mongo_lsv.list_dbs())
print("--------------------")
print(mongo_lsv.list_collections(db_name="communications"))
print("--------------------------------------------------------")
print(
    mongo_lsv.get_records_from_collection(
        db_name="communications", collection="type_of_media"
    )
)
print("--------------------------------------------------------------------------")
print(
    mongo_lsv.get_records_from_collection(
        db_name="communications", collection="media"
    )
)