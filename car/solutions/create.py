from bson.objectid import ObjectId
import sys
sys.path.append("..")
from databases.client import MongoLsv

mongo_lsv = MongoLsv()
""" print(
    mongo_lsv.create_new_record_in_collection(
        db_name="communications",
        collection="type_of_media",
        record={"name": "Medios Masivos"},
    )
) 

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="communications",
        collection="type_of_media",
        record={"name": "Medios Auxiliares o complementarios"},
    )
) 

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="communications",
        collection="type_of_media",
        record={"name": "Medios Alternativos"},
    )
)  """

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="communications",
        collection="media",
        record={"name": "Internet", "type_of_media_id":ObjectId("62afb84a1214775c067995e1")},
    )
) 

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="communications",
        collection="media",
        record={"name": "Television", "type_of_media_id":ObjectId("62afb84a1214775c067995e1")},
    )
) 

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="communications",
        collection="media",
        record={"name": "Radio", "type_of_media_id":ObjectId("62afb84a1214775c067995e1")},
    )
) 

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="communications",
        collection="media",
        record={"name": "Periodicos", "type_of_media_id":ObjectId("62afb84a1214775c067995e1")},
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="communications",
        collection="media",
        record={"name": "Revistas", "type_of_media_id":ObjectId("62afb84a1214775c067995e1")},
    )
) 

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="communications",
        collection="media",
        record={"name": "Cine", "type_of_media_id":ObjectId("62afb84a1214775c067995e1")},
    )
) 

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="communications",
        collection="media",
        record={"name": "Medios en exteriores", "type_of_media_id":ObjectId("62afb84a1214775c067995e2")},
    )
) 

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="communications",
        collection="media",
        record={"name": "Publicidad Interior", "type_of_media_id":ObjectId("62afb84a1214775c067995e2")},
    )
) 

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="communications",
        collection="media",
        record={"name": "Publicidad Directa", "type_of_media_id":ObjectId("62afb84a1214775c067995e2")},
    )
) 

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="communications",
        collection="media",
        record={"name": "Faxes", "type_of_media_id":ObjectId("62afb84a1214775c067995e3")},
    )
) 

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="communications",
        collection="media",
        record={"name": "Discos compactos", "type_of_media_id":ObjectId("62afb84a1214775c067995e3")},
    )
) 

print(mongo_lsv.list_dbs())
print("--------------------")
print(mongo_lsv.list_collections(db_name="communications"))
print("--------------------------------------------------")
print(
    mongo_lsv.get_records_from_collection(
        db_name="communications", collection="type_of_media"
    )
)
print("----------------------------------------------------------")
print(
    mongo_lsv.get_records_from_collection(
        db_name="communications", collection="media"
    )
)