from bson.objectid import ObjectId
import sys
sys.path.append("..")
from databases.client import MongoLsv

mongo_lsv = MongoLsv()

print(
    mongo_lsv.delete_record_in_collection(
        db_name="communications",
        collection="type_of_media",
        record_id="62afb84a1214775c067995e3",
    )
) 

print(
    mongo_lsv.delete_record_in_collection(
        db_name="communications",
        collection="media",
        record_id="62afbab040e36081bcba1e8c",
    )
) 

print(mongo_lsv.list_dbs())
print("--------------------")
print(mongo_lsv.list_collections(db_name="communications"))
print("--------------------------------------------------------")
print(
    mongo_lsv.get_records_from_collection(
        db_name="communications", collection="type_of_media"
    )
)
print("--------------------------------------------------------------------------------------")
print(
    mongo_lsv.get_records_from_collection(
        db_name="communications", collection="media"
    )
)